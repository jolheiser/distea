module code.gitea.io/distea

go 1.17

require (
	code.gitea.io/sdk/gitea v0.15.1
	github.com/diamondburned/arikawa/v3 v3.0.0-rc.5
	github.com/google/go-github/v43 v43.0.0
	github.com/matryer/is v1.4.0
	github.com/peterbourgon/ff/v3 v3.1.2
	github.com/rs/zerolog v1.26.1
	golang.org/x/oauth2 v0.0.0-20220309155454-6242fa91716a
)

require (
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/gorilla/schema v1.2.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hashicorp/go-version v1.2.1 // indirect
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/crypto v0.0.0-20211215165025-cf75a172585e // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
