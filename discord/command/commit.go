package command

import (
	"fmt"
	"strconv"

	"github.com/diamondburned/arikawa/v3/api"
	"github.com/diamondburned/arikawa/v3/discord"
	"github.com/rs/zerolog/log"
)

func init() {
	Commands["commit"] = Commit
}

// Commit is the command for getting a forge.Commit by its SHA
var Commit = Command{
	Meta: api.CreateCommandData{
		Name:        "commit",
		Description: "Commit information",
		Type:        discord.ChatInputCommand,
		Options: []discord.CommandOption{
			&discord.StringOption{
				OptionName:  "sha",
				Description: "The commit SHA (partial or full)",
				Required:    true,
			},
		},
	},
	Handler: func(ctx *Context) {
		if err := ctx.State.RespondInteraction(ctx.Event.ID, ctx.Event.Token, deferredInteraction); err != nil {
			log.Err(err).Msg("")
			return
		}

		sha := ctx.Event.Data.(*discord.CommandInteraction).Options[0].String()

		commit, err := ctx.Forge.Commit(sha)
		if err != nil {
			log.Err(err).Msg("")
			ctx.DeferredError("could not retrieve commit")
			return
		}

		body := ctx.Forge.ReplaceIssueLinks(commit.Message)
		if len(body) > 1000 {
			body = body[:1000] + "..."
		}
		embed := discord.Embed{
			Author: &discord.EmbedAuthor{
				Name: commit.Author.Username,
				URL:  commit.Author.URL,
				Icon: commit.Author.Image,
			},
			Thumbnail: &discord.EmbedThumbnail{
				URL: ctx.Forge.Image(),
			},
			Title:       commit.SHA,
			URL:         commit.URL,
			Description: body,
			Footer: &discord.EmbedFooter{
				Text: "Created",
			},
			Timestamp: discord.NewTimestamp(commit.Time),
			Fields: []discord.EmbedField{
				{
					Name:   "Stats",
					Value:  fmt.Sprintf("%d ++ %d --", commit.Additions, commit.Deletions),
					Inline: true,
				},
				{
					Name:   "Verified",
					Value:  strconv.FormatBool(commit.Verified),
					Inline: true,
				},
			},
		}

		data := api.EditInteractionResponseData{
			Embeds: &[]discord.Embed{
				embed,
			},
		}

		if _, err := ctx.State.EditInteractionResponse(ctx.App.ID, ctx.Event.Token, data); err != nil {
			log.Err(err).Msg("")
		}
	},
}
