package command

import (
	"github.com/diamondburned/arikawa/v3/api"
	"github.com/diamondburned/arikawa/v3/discord"
	"github.com/diamondburned/arikawa/v3/utils/json/option"
	"github.com/rs/zerolog/log"
)

func init() {
	Commands["debug"] = Debug
}

// Debug is the debugging command
var Debug = Command{
	Meta: api.CreateCommandData{
		Name:        "debug",
		Description: "Debugging info",
		Type:        discord.ChatInputCommand,
	},
	Handler: func(ctx *Context) {
		data := api.InteractionResponse{
			Type: api.MessageInteractionWithSource,
			Data: &api.InteractionResponseData{
				Content: option.NewNullableString(debugMessage),
			},
		}
		if err := ctx.State.RespondInteraction(ctx.Event.ID, ctx.Event.Token, data); err != nil {
			log.Err(err).Msg("")
		}
	},
}

const debugMessage = `1. We absolutely need to know what version of Gitea you're running.
2. We need to know what architecture and platform you're running gitea from and if you're using docker or systemd or other things. We need to know how you start it.
3. 🪵 LOGS 🪵
    See <https://docs.gitea.io/en-us/logging-configuration/#debugging-problems> for how to set up DEBUG logs.
    See <https://docs.gitea.io/en-us/faq/#stdout-logging-on-systemd-and-docker> for how to see stdout logging.
4. app.ini - please censor KEYs, passwords and other confidential information but it really helps to see the whole app.ini instead of segments.`
